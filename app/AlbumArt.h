#ifndef ALBUMART_H
#define ALBUMART_H

#include <QObject>
#include <QDebug>
#include <qtappfw/messageengine.h>
#include <qtappfw/mediascanner.h>

#if 1

#endif
#if 0
static void demo_func (struct afb_req request)
{
    json_object *jresp = NULL;

    if (jresp == NULL) {
        afb_req_fail(request, "failed", "media parsing error");
        return;
    }

    afb_req_success(request, jresp, "Media Results Displayed");
}

static const struct afb_verb_v2 binding_verbs[] = {
    // { .verb = "media_result", .callback = media_results_get, .info = "Media scan result" },
    // { .verb = "subscribe",    .callback = subscribe,         .info = "Subscribe for an event" },
    // { .verb = "unsubscribe",  .callback = unsubscribe,       .info = "Unsubscribe for an event" },
    // // DH.KIM 2019.01.11.
    // { .verb = "get_file_list",.callback = get_file_list,     .info = "Search file list" },
    // { .verb = "get_directory_list",.callback = get_directory_list, .info = "Search directory list" },
           { .verb = "demo", .callback = demo_func, .info = "demo result" },
    { }
};
#endif



class AlbumArt : public QObject
{
    Q_OBJECT
public:
    explicit AlbumArt(QObject *parent = nullptr);

    bool getStatus();

signals:

public slots:



private:
    bool m_scanned;
};

#endif // ALBUMART_H

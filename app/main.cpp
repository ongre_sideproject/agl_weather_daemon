#include <QCoreApplication>
#include "AlbumArt.h"

#include <QJsonDocument>
#include <QJsonObject>

#if 0
const char *api;			/* api name for the binding */
const char *specification;		/* textual specification of the binding, can be NULL */
const char *info;			/* some info about the api, can be NULL */
const struct afb_verb_v2 *verbs;	/* array of descriptions of verbs terminated by a NULL name */
int (*preinit)();                       /* callback at load of the binding */
int (*init)();                          /* callback for starting the service */
void (*onevent)(const char *event, struct json_object *object); /* callback for handling events */
unsigned noconcurrency: 1;		/* avoids concurrent requests to verbs */
#endif
#ifndef WIN32
extern "C"
{
#define AFB_BINDING_VERSION 2
//#include <afb/afb-wsj1.h>
//#include <afb/afb-ws-client.h>
#include <afb/afb-binding.h>
//#include <afb/afb-binding.hpp>
#include <json-c/json.h>
}


AlbumArt *albumArt;

bool convertJson(json_object *value ,const char *ch)
{
    value = json_object_new_string(ch);
    return true;
}





void get_status(afb_req request) noexcept
{
    QJsonObject object;
    object.insert("Status", QJsonValue::fromVariant(albumArt->getStatus()));
    QJsonDocument(object).toJson(QJsonDocument::Compact).toStdString().c_str();

    json_object *resprespons;
    convertJson(resprespons,QJsonDocument(object).toJson(QJsonDocument::Compact).toStdString().c_str());

    afb_req_success(request, resprespons, "get_scan_status success");

    //albumArt->getStatus()
    //json_object
    /*
    json_object *jarray = json_object_new_array();
    json_object *jresp = json_object_new_object();
    json_object *jdict = json_object_new_object();
    json_object *pre_scanned_jstring = json_object_new_string("PRE_SCANNED");
    json_object *full_scanned_jstring = json_object_new_string("FULL_SCANNED");

    printf("get_scan_status start\n");

    int scan_status = get_scan_status_flag();

    if (scan_status == PRESCANNED)
    {
        json_object_object_add(jdict, "scan_status", pre_scanned_jstring);
    }
    else if (scan_status == FULLSCANNED)
    {
        json_object_object_add(jdict, "scan_status", full_scanned_jstring);
    }

    json_object_array_add(jarray, jdict);

    json_object_object_add(jresp, "scan_status", jarray);

    afb_req_success(request, jresp, "get_scan_status success");

    printf("get_scan_status success\n");
    */

    return;
}
static int preinit() noexcept
{
    qDebug() << "preinit";
    return 0;
}

static int init() noexcept
{
    qDebug() << "init";
    albumArt =  new AlbumArt;
    return 0;
}

const struct afb_verb_v2 albumArt_verbs[] = {

    {"get_status", get_status, nullptr, nullptr, AFB_SESSION_NONE},
};

extern "C" const struct afb_binding_v2 afbBindingV2 =
{
    "AlbumArtAPI", //name
    nullptr, //
    "AlbumArtAPI for jhyoo", //
    albumArt_verbs, //verbs
    preinit, //
    init, //init
    nullptr,//
    0//
};
#else
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    RSEWeather app;


    return a.exec();
}
#endif

QT += core network websockets
QT -= gui

CONFIG += console link_pkgconfig c++11
CONFIG -= app_bundle

DEFINES += QT_DEPRECATED_WARNINGS

TARGET = weather_service
!win32{
    include(app.pri)
    TEMPLATE = lib
    CONFIG += plugin

}
win32{
    TEMPLATE = app

    FILE = $$PWD/app.pri
    CONTENT = $$cat($$FILE,lines)
    message($$replace(CONTENT, package, test))

    var = $$system(git rev-parse --short HEAD)
    filepath = $$PWD/1.text
    message(Variable: $$var and filename: $$filepath)
    !write_file($$filepath, var) {
      warning(Cant create a file)
    }

}


SOURCES += \
    AlbumArt.cpp \
    main.cpp

HEADERS  += \
    AlbumArt.h

 
target.path = /home/root
INSTALLS += target



#ifndef WEATHER_H
#define WEATHER_H

#include <QString>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QObject>
#include <QDebug>
#include "darksky_weather.h"
#include "openweathermap_weather.h"
#include "aerisweather.h"



enum WeatherAPI
{
    NoneAPI = 0,
    DarkSky,
    OpenWeatherMap,
    AERIS
};


class Weather
{
public:
    explicit Weather(WeatherAPI api, QString apikey);

    QString weatherDescription();
    QString temperature();
    QString city();
    QString poweredby(); // Some API are required display Powered By

    bool getWeatherData(double latitude, double longitude);
    bool getWeatherData(QString location = "");

private:
    bool sendToApi(QString url);

    QString m_url; // Api url
    QByteArray data; // Api json data
    WeatherAPI m_API; // Current Api
    QString m_apiKey;
    void *weatherClass;
};

#endif // WEATHER_H

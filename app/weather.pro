QT -= gui
QT += network core websockets

CONFIG += c++11 console
CONFIG -= app_bundle

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    aerisweather.cpp \
    darksky_weather.cpp \
    weather.cpp \
    openweathermap_weather.cpp \
    RESWeather.cpp \
    main.cpp

HEADERS  += \
    aerisweather.h \
    darksky_weather.h \
    weather.h \
    openweathermap_weather.h \
    RESWeather.h


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


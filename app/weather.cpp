#include "weather.h"

Weather::Weather(WeatherAPI api, QString apikey)
{
    m_API = api;
    m_apiKey = apikey;

    switch(m_API)
    {
    case DarkSky:
        m_url = QString("https://api.darksky.net/forecast/"); break;
    case OpenWeatherMap:
        m_url = QString("http://api.openweathermap.org/data/2.5/weather?lat="); break;
    case AERIS:
        m_url = QString("http://api.aerisapi.com/observations/"); break;
    default: break;
    }
}

bool Weather::sendToApi(QString url)
{
    QEventLoop eventLoop;
    QNetworkAccessManager nam;
    QObject::connect(&nam, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));


    QUrl weather_url = QUrl(url);
    QNetworkRequest req(weather_url);
    QNetworkReply *reply = nam.get(req);
    eventLoop.exec();

    if(reply->error())
    {
        qDebug() << "ERROR Get weather data : " << reply->errorString();
        return false;
    }

    data = reply->readAll();
    switch(m_API)
    {
    case DarkSky:
        weatherClass = new aerisWeather(QString(data.toStdString().c_str()).toUtf8()); break;
    case OpenWeatherMap:
        weatherClass = new openweathermap_weather(QString(data.toStdString().c_str()).toUtf8()); break;
    case AERIS:
        weatherClass = new aerisWeather(QString(data.toStdString().c_str()).toUtf8()); break;
    default: break;
    }
    qDebug() << data;
    return true;
}

bool Weather::getWeatherData(double latitude, double longitude)
{
    QString url;
    switch(m_API)
    {
        case DarkSky:
            url = m_url + QString(m_apiKey) + QString("/") + QString::number(latitude) + QString(",") + QString::number(longitude);
            break;

        case OpenWeatherMap:
            url = m_url + QString::number(latitude) + QString("&lon=") + QString::number(longitude) + QString("&appid=") + QString(m_apiKey);
            break;
        case AERIS:
            return false;

        default:
            return false;
    }

    qDebug() << url;
    return sendToApi(url);
}
bool Weather::getWeatherData(QString location)
{
    QString url;
    switch(m_API)
    {
        case DarkSky:
        case OpenWeatherMap:
            return false;
        case AERIS:
        url = m_url + location + QString("?") + QString(m_apiKey);
        break;

        default:
            return false;
    }

    qDebug() << url;
    return sendToApi(url);
}


QString Weather::weatherDescription()
{
    if(m_API == NoneAPI)
    {
        return QString();
    }
    if(data.isEmpty())
    {
        return QString();
    }

    switch(m_API)
    {
        case DarkSky:
            return static_cast<darksky_weather*>(weatherClass)->weatherDescription();
        case OpenWeatherMap:
            return static_cast<openweathermap_weather*>(weatherClass)->weatherDescription();;
        case AERIS:
            return static_cast<aerisWeather*>(weatherClass)->weatherDescription();
        default:
            return QString();
    }
}

QString Weather::temperature()
{
    if(m_API == NoneAPI)
    {
        return QString();
    }
    if(data.isEmpty())
    {
        return QString();
    }

    switch(m_API)
    {
        case DarkSky:
            return static_cast<darksky_weather*>(weatherClass)->temperature();
        case OpenWeatherMap:
            return static_cast<openweathermap_weather*>(weatherClass)->temperature();;
        case AERIS:
            return static_cast<aerisWeather*>(weatherClass)->temperature();
        default:
            return QString();
    }
}

QString Weather::city()
{
    if(m_API == NoneAPI)
    {
        return QString();
    }
    if(data.isEmpty())
    {
        return QString();
    }

    switch(m_API)
    {
        case DarkSky:
            return static_cast<darksky_weather*>(weatherClass)->city();
        case OpenWeatherMap:
            return static_cast<openweathermap_weather*>(weatherClass)->city();;
        case AERIS:
            return static_cast<aerisWeather*>(weatherClass)->city();
        default:
            return QString();
    }


}

QString Weather::poweredby()
{
    if(m_API == NoneAPI)
    {
        return QString();
    }
    if(data.isEmpty())
    {
        return QString();
    }

    switch(m_API)
    {
        case DarkSky:
            return static_cast<darksky_weather*>(weatherClass)->poweredby();
        case OpenWeatherMap:
            return static_cast<openweathermap_weather*>(weatherClass)->poweredby();;
        case AERIS:
            return static_cast<aerisWeather*>(weatherClass)->poweredby();
        default:
            return QString();
    }

}
